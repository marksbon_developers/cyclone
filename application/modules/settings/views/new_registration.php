 <!-- Content area --> 
  <div class="content" style="background:url(<?=base_url()?>resources/images/backgrounds/bg.png);">
    <!-- Main charts -->
    <div class="row">
      <div class="col-md-1">

        <!-- Main sidebar -->
      <div class="sidebar sidebar-main sidebar-default">
        <div class="sidebar-content">
          <!-- User menu -->
          <div class="sidebar-user-material">
            <div class="category-content">
              <div class="sidebar-user-material-content">


                <h6>Victoria Baker</h6>
                <span class="text-size-small">Santa Ana, CA</span>
              </div>
                            
              <div class="sidebar-user-material-menu">
                <a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
              </div>
            </div>
            
            <div class="navigation-wrapper collapse" id="user-nav">
              <ul class="navigation">
                <li><a href="#"><i class="icon-user-plus"></i> <span>My profile</span></a></li>
                <li><a href="#"><i class="icon-coins"></i> <span>My balance</span></a></li>
                <li><a href="#"><i class="icon-comment-discussion"></i> <span><span class="badge bg-teal-400 pull-right">58</span> Messages</span></a></li>
                <li class="divider"></li>
                <li><a href="#"><i class="icon-cog5"></i> <span>Account settings</span></a></li>
                <li><a href="#"><i class="icon-switch2"></i> <span>Logout</span></a></li>
              </ul>
            </div>
          </div>
          <!-- /user menu -->
          <!-- Main navigation -->
          <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
              <ul class="navigation navigation-main navigation-accordion">

                <!-- Main -->
                <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                <li>
                  <a href="<?= base_url() ?>dashboard/index"><i class="icon-grid6"></i> <span>Dashboard</span></a>
                </li>
               <li>
                  <a href="#"><i class="icon-indent-decrease2"></i> <span>cash In</span></a>
                </li>
                <li>
                  <a href="#"><i class="icon-indent-increase2"></i> <span>cash out</span></a>
                </li>
                <li>
                  <a href="<?= base_url() ?>settings/new_registration"><i class="icon-stack2"></i> <span>Settings</span></a>
                  
                </li>
                <li>
                  <a href="#"><i class="icon-switch2"></i> <span>Layouts</span></a>
                </li>
                
              

              </ul>
            </div>
          </div>
          <!-- /main navigation -->

        </div>
      </div>
      <!-- /main sidebar -->
      </div>
     <!-- Content area --> 

    <!-- Main charts -->
      <div class="col-lg-8 col-md-10">
      
        <div class="col-md-3 col-xs-6">
          <div class="content-group">
            <div class="row row-seamless btn-block-group">
              <div class="col-xs-12">
                <a href="<?=base_url()?>inhouse/Classifications" type="button" class="btn btn-default btn-block btn-float btn-float-lg">
                 <i class="icon-versions"></i>
                  <span>Classifications</span>
                </a>
              </div>
            </div>
          </div> 
        </div>
        <div class="col-md-3 col-xs-6">
          <div class="content-group">
            <div class="row row-seamless btn-block-group">
              <div class="col-xs-12">
                <a href="<?=base_url()?>inhouse/payee" type="button" class="btn btn-default btn-block btn-float btn-float-lg">
                 <i class=" icon-user-tie"></i>
                  <span>Payee</span>
                </a>
              </div>
            </div>
          </div> 
        </div>
        <div class="col-md-3 col-xs-6">
          <div class="content-group">
            <div class="row row-seamless btn-block-group">
              <div class="col-xs-12">
                <a href="<?=base_url()?>inhouse/Issuer" type="button" class="btn btn-default btn-block btn-float btn-float-lg">
                 <i class=" icon-user-tie"></i>
                  <span>Issuer</span>
                </a>
              </div>
            </div>
          </div> 
        </div>
        <div class="col-md-3 col-xs-6">
          <div class="content-group">
            <div class="row row-seamless btn-block-group">
              <div class="col-xs-12">
                <a href="<?=base_url()?>inhouse/Bank" type="button" class="btn btn-default btn-block btn-float btn-float-lg">
                 <i class="icon-piggy-bank"></i>
                  <span>Bank</span>
                </a>
              </div>
            </div>
          </div> 
        </div>

      </br><br/>

      <div class="col-md-3 col-xs-6">
          <div class="content-group">
            <div class="row row-seamless btn-block-group">
              <div class="col-xs-12">
                <a href="<?=base_url()?>inhouse/signaturies" type="button" class="btn btn-default btn-block btn-float btn-float-lg">
                 <i class="icon-compose"></i>
                  <span>Signaturies</span>
                </a>
              </div>
            </div>
          </div> 
        </div>
        <div class="col-md-3 col-xs-6">
          <div class="content-group">
            <div class="row row-seamless btn-block-group">
              <div class="col-xs-12">
                <a href="<?=base_url()?>inhouse/users" type="button" class="btn btn-default btn-block btn-float btn-float-lg">
                 <i class="icon-people"></i>
                  <span>Users</span>
                </a>
              </div>
            </div>
          </div> 
        </div>
        <div class="col-md-3 col-xs-6">
          <div class="content-group">
            <div class="row row-seamless btn-block-group">
              <div class="col-xs-12">
                <a href="<?=base_url()?>settings/page_settings" type="button" class="btn btn-default btn-block btn-float btn-float-lg">
                 <i class="icon-puzzle3"></i>
                  <span>Company Setting</span>
                </a>
              </div>
            </div>
          </div> 
        </div>
        <div class="col-md-3 col-xs-6">
          <div class="content-group">
            <div class="row row-seamless btn-block-group">
              <div class="col-xs-12">
                <a href="<?=base_url()?>reports/index" type="button" class="btn btn-default btn-block btn-float btn-float-lg">
                 <i class=" icon-file-stats2"></i>
                  <span>Reports</span>
                </a>
              </div>
            </div>
          </div> 
        </div>
     
      </div>
     
      <div class="col-lg-1 col-md-1"></div>
       <div class="col-lg-2 col-md-2" style="margin-left:-40px;">
        <div class="sidebar-detached">
          <div class="sidebar sidebar-default">
            <div class="sidebar-content">
              <div class="sidebar-category no-margin">
                <div class="category-title" style="padding: 5px 5px 5px 14px;">
                  <h3 style="margin-top: 10px;"><?=$_SESSION['companyinfo']['name']?> </h3>
                  <ul class="nav navigation">
                    <!-- <li>Incooperation Date:<span class="text-muted text-regular "><b>21-10-2015</b> </span></li> -->
                    <li>TIN Number:<span class="text-muted text-regular "><br/><b> <?=@$_SESSION['companyinfo']['tin_number']?></b> </span></li>
                    <li>Postal Address:<span class="text-muted text-regular"><br/><b><?=@$_SESSION['companyinfo']['postal_address']?></b></span></li>
                    <li>Residence Address:<span class="text-muted text-regular"><br/><b><?=@$_SESSION['companyinfo']['residence_address']?></b></span></li>
                    <li>Email Address:<span class="text-muted text-regular"><br/><b><?=@$_SESSION['companyinfo']['email']?></b></span></li>
                    <li>Website Address:<span class="text-muted text-regular"><br/><b><?=@$_SESSION['companyinfo']['website']?></b></span></li>
                  </ul>
                </div>
              </div>
              <div class="sidebar-category">
                <div class="category-content no-padding">
                  <ul class="nav navigation" >
                    <li ><a href="#v_1_1"><i class="icon-bubbles4 text-slate-400" style="color: #333"></i ><p style="color:#333;">Vision:</p><span class="text-muted text-regular pull-left"><?=@$_SESSION['companyinfo']['vision']?></span></a></li>
                    <li class="navigation-divider"></li>
                   <li><a href="#v_1_1"><i class="icon-footprint text-slate-400"></i><p style="color:##333;">Mision:</p><span class="text-muted text-regular pull-left"><?=@$_SESSION['companyinfo']['mission']?></span></a></li>
                    <li class="navigation-divider"></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /main charts -->



