 <!-- Content area --> 
  <div class="content" style="background:url(<?=base_url()?>resources/images/backgrounds/bg.png);">
    <!-- Main charts -->
    <div class="row">
      <div class="col-md-1">
        <!-- Main sidebar -->
      <div class="sidebar sidebar-main sidebar-default">
        <div class="sidebar-content">

          <!-- User menu -->
          <div class="sidebar-user-material">
            <div class="category-content">
              <div class="sidebar-user-material-content">


                <h6>Victoria Baker</h6>
                <span class="text-size-small">Santa Ana, CA</span>
              </div>
                            
              <div class="sidebar-user-material-menu">
                <a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
              </div>
            </div>
            
            <div class="navigation-wrapper collapse" id="user-nav">
              <ul class="navigation">
                <li><a href="#"><i class="icon-user-plus"></i> <span>My profile</span></a></li>
                <li><a href="#"><i class="icon-coins"></i> <span>My balance</span></a></li>
                <li><a href="#"><i class="icon-comment-discussion"></i> <span><span class="badge bg-teal-400 pull-right">58</span> Messages</span></a></li>
                <li class="divider"></li>
                <li><a href="#"><i class="icon-cog5"></i> <span>Account settings</span></a></li>
                <li><a href="#"><i class="icon-switch2"></i> <span>Logout</span></a></li>
              </ul>
            </div>
          </div>
          <!-- /user menu -->


          <!-- Main navigation -->
          <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
              <ul class="navigation navigation-main navigation-accordion">

                <!-- Main -->
                <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                <li>
                  <a href="<?= base_url() ?>dashboard/index"><i class="icon-grid6"></i> <span>Dashboard</span></a>
                </li>
               <li>
                  <a href="<?= base_url() ?>dashboard/cashin"><i class="icon-indent-decrease2"></i> <span>cash In</span></a>
                </li>
                <li>
                  <a href="<?= base_url() ?>dashboard/cashout"><i class="icon-indent-increase2"></i> <span>cash out</span></a>
                </li>
                <li>
                  <a href="<?= base_url() ?>settings/new_registration"><i class="icon-stack2"></i> <span>Settings</span></a>
                  
                </li>
                <li>
                  <li><a href="<?=base_url()?>access/logout"><i class="icon-switch2"></i> Logout</a></li>
                </li>
                
              

              </ul>
            </div>
          </div>
          <!-- /main navigation -->

        </div>
      </div>
      <!-- /main sidebar -->
      </div>
    <div class="col-lg-7 col-md-7">

        <div class="sidebar-content">
          <div class="sidebar-category no-margin">
          <div class="category-title" style="padding: 5px 5px 5px 14px;">
          <h3 style="margin-top: 10px;">CASH IN </h3>
           <div class="col-xs-12" style="background:#1a222f;height: 5px"></div>
           <div class="panel panel-flat">
            <div class="panel-body">
                  <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-top">
                      <li class="active"><a href="#top-tab1" data-toggle="tab">All Cash in Records</a></li>
                      <li><a href="#top-tab2" data-toggle="tab">Add New</a></li>
                     
                    </ul>

                    <div class="tab-content">
                      <div class="tab-pane active" id="top-tab1">
                          <table id="allpending_orders" class="table table-responsive table-xs">
                       <thead>
                      <tr class="bg-teal-400">
                           <th> Name</th>
                           <th>Company</th>
                           <th>Email</th>
                           <th>Phone</th>
                           <th>Status</th>
                           <th class="text-center">Actions</th>
                         </tr>
                       </thead>
                       <tbody>
                       </tbody>
                     </table>
                      </div>

                      <div class="tab-pane" id="top-tab2">
                       <div class="col-xs-6" >
              <div class="form-group has-feedback has-feedback-left">
                <label style="margin-bottom: -15px;">Amount *</label>
                <input type="email" class="form-control" placeholder="Start typing here" oncopy="return false;" onpaste="return false;" onselectstart="return false;" autocomplete="off" name="email" required >
                <div class="form-control-feedback">
                </div>
              </div>
              <div class="form-group has-feedback has-feedback-left">
                 <label style="margin-bottom: -15px;">Cheque Number*</label>
                <input type="email" class="form-control" placeholder="Start typing here" oncopy="return false;" onpaste="return false;" onselectstart="return false;" autocomplete="off" name="email" required >
                <div class="form-control-feedback">
                </div>
              </div>
              <div class="form-group has-feedback has-feedback-left">
                <label style="margin-bottom: -15px;">Received By*</label>
                <input type="email" class="form-control" placeholder="Start typing here" oncopy="return false;" onpaste="return false;" onselectstart="return false;" autocomplete="off" name="email" required >
                <div class="form-control-feedback">
                </div>
              </div>
            
            </div>
            <div class="col-xs-6" >
             <div class="form-group has-feedback has-feedback-left">
               <label style="margin-bottom: -15px;">Bank *</label>
                <select class="form-control" name="priority">
                        <option value=""></option>
                        <option value="normal">Normal</option>
                        <option value="urgent">Urgent</option>  </select>
                <div class="form-control-feedback">
                </div>
              </div> 
              <div class="form-group has-feedback has-feedback-left">
               <label style="margin-bottom: -15px;">Cashed by *</label>
                <select class="form-control" name="priority">
                        <option value=""></option>
                        <option value="normal">Normal</option>
                        <option value="urgent">Urgent</option>  </select>
                <div class="form-control-feedback">
                </div>
              </div>
              
              <div class="form-group has-feedback has-feedback-left">
               <label style="margin-bottom: -15px;">Cheque </label>
                <input oncopy="return false;" onpaste="return false;" onselectstart="return false;" type="file" name="receipt_file" value="" class="form-control" placeholder="" autocomplete="off" oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Invalid Entry')">
                <div class="form-control-feedback">
                </div>
              </div>
            </div>
            <div class="col-xs-12">
               <div class="col-xs-6"></div>
               <div class="col-xs-6">
                 <div class="col-xs-3"><button type="submit" name="login" class="btn bg-pink-200 btn-block pull-right">Cancel </button></div>
                 <div class="col-xs-3"><button type="submit" name="login" class="btn bg-pink-200 btn-block">Submit </button></div>
               </div>
               
            
            </div>
                      </div>
                    </div>
                  </div>
                </div>
          </div>
        </div>
        </div>
        </div>
      </div>
      <div class="col-lg-1 col-md-1"></div>
      <div class="col-lg-3 col-md-2" style="margin-left:-40px;">
        <div class="sidebar-content">
          <div class="sidebar-category no-margin">
          <div class="category-title" style="padding: 5px 5px 5px 14px;">
          <h3 style="margin-top: 10px;">CASH AT HAND </h3>
           <div class="col-xs-12" style="background:#1a222f;height: 5px"></div>
           <div class="panel panel-flat">
            <div class="panel-body">
              <div class="form-group has-feedback has-feedback-left">
             <table class="table">
                <thead>
                  <tr>
                    <th style="border: 0px"></th>
                    <th style="border: 0px"></th>
                    <th style="border: 0px"></th>
                    <th style="border: 0px"></th>
                    <th style="border: 0px"></th>
                    <th  style="font-size: 30px;border: 0px">600<sup style="font-size: 17px;margin-bottom: 17px">.00</sup><p style="font-size: 13px">Transactions</p></th>
                  </tr>
                  <tr>
                    <th colspan="2"><strong>Classification</strong></th>
                    <th></th>
                     <th></th>
                     <th></th>
                    <th>Amount</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width:5px" ><i class="icon-arrow-left16 position-left"></i></td>
                    <td style="width:100px">nana</td>
                    <td style="width:50px" ></td>
                     <td ></td>
                     <td ></td>
                    <td >100</td>
                  </tr>
                  <tr>
                    <td style="width:5px"><i class="icon-arrow-right16 position-left"></i></td>
                    <td style="width:100px"">pertty cash</td>
                    <td style="width:50px"></td>
                    <td></td>
                    <td ></td>
                    <td>100</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        </div>
        </div>
      </div>

    </div>
    <!-- /main charts -->



